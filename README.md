# Wafers Classification
This project is a classical Image classification problem, but with an emphasis on a very unbalanced dataset, which has to be taken care of. 


## Getting started
Make sure to use the wafers_image dataset provided by kaggle. 

## Data Augmentation Approaches 

-  Use of ImageDataGenerator to create new images by applying turning, adding noise, ...
-  Use of Encoder/Decoder- Neural Network to learn features of images and then create new ones
